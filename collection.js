// collection.js

module.exports = {
    create: function (index, $function) {
        var newArr = [];
        for (var i = 0; i < index; i++) {
            newArr.push($function(i));
        }
        return newArr;
    },
    map: function (arr, $function) {
        return arr.map($function);
    },
    every: function (arr, isBig) {
        return arr.every(isBig);
    },
    none: function (arr, isCool) {
        return !arr.some(isCool);
    },
    uniq: function (arr, $function) {
        var newArr = arr.map($function);
        var unique = newArr.filter(function (value, index, array) {
            return array.indexOf(value) === index;
        });
        return unique;
    },
    add: function (arr, ...list) {
        return arr.concat(list);
    }
};
